import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EgresosRepository} from '../../../../domain/repository/EgresosRepository';
import {NetworkCallBack} from '../../../../domain/entity/NetworkCallBack';
import Swal from 'sweetalert2';
import {MatDialogRef} from '@angular/material/dialog';
import {Egreso} from '../../../../domain/entity/Egreso';
import {AuthService} from '../../../../framework/services/auth.service';

@Component({
  selector: 'app-save-egreso-dialog',
  templateUrl: './save-egreso-dialog.component.html',
  styleUrls: ['./save-egreso-dialog.component.css']
})
export class SaveEgresoDialogComponent implements OnInit {
  private ecargado: string = '';
  formEgreso: FormGroup;

  constructor(
    private fb: FormBuilder,
    private egresoRepository: EgresosRepository,
    private dialogRef: MatDialogRef<SaveEgresoDialogComponent>,
    private authService: AuthService
  ) {
    this.ecargado = `${authService.getUser().name} ${authService.getUser().lastname}`;
    this.formEgreso = this.fb.group({
      persona: [this.ecargado, [Validators.required]],
      fecha: [new Date(), [Validators.required]],
      monto: ['', [Validators.required,Validators.min(0)]],
      motivo: ['', [Validators.required]],
    });
  }


  ngOnInit(): void {
  }

  saveEgreso() {
    this.dialogRef.close();
    const parent = this;

    if (this.formEgreso.valid) {

      const value = this.formEgreso.value;

      const date = value.fecha instanceof Date ? value.fecha.getTime() : new Date(value.fecha).getTime();

      const egreso: Egreso = {
        id: value.id,
        fecha: date,
        monto: value.monto,
        persona: value.persona,
        motivo: value.motivo,
      };

      this.egresoRepository.save(egreso, new class implements NetworkCallBack<boolean> {
        onComplete(result: boolean) {
          Swal.fire('Egreso registrado exitosamente', '', 'success');

        }

        onException(e: Error) {
          Swal.fire('Ocurrio un error', e.message, 'error');
        }
      });
    }
  }

}
