import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveEgresoDialogComponent } from './save-egreso-dialog.component';

describe('SaveEgresoDialogComponent', () => {
  let component: SaveEgresoDialogComponent;
  let fixture: ComponentFixture<SaveEgresoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveEgresoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveEgresoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
