import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Egreso} from '../../../domain/entity/Egreso';
import {MatDialog} from '@angular/material/dialog';
import {SaveEgresoDialogComponent} from './save-egreso-dialog/save-egreso-dialog.component';
import {EgresosMVP} from '../../../domain/mvpdefinition/EgresosMVP';
import Swal from 'sweetalert2';
import {NgForm} from '@angular/forms';
import {Moment} from 'moment';

@Component({
  selector: 'app-egresos',
  templateUrl: './egresos.component.html',
  styleUrls: ['./egresos.component.css']
})
export class EgresosComponent implements OnInit, EgresosMVP.View {

  dataSource: MatTableDataSource<Egreso>;
  displayedColumns: string[] = ['persona', 'fecha', 'monto', 'motivo'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  fecheInicio: any;
  fechaFin: any;

  constructor(
    public dialog: MatDialog,
    private presenter: EgresosMVP.Presenter
  ) {

  }

  ngOnInit(): void {
    this.presenter.setView(this);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialogRegistroEgreso() {
    const dialogRef = this.dialog.open(SaveEgresoDialogComponent, {
      width: '350px',
    });
  }

  loadEgresosByDateRange(startDate, endingDate) {
    this.presenter.loadEgresosByDate(startDate, endingDate);
  }

  onloadEgresosByDateRangeReady(egresos: Egreso[]) {
    this.dataSource = new MatTableDataSource<Egreso>(egresos);
    this.dataSource.sort = this.sort;
  }

  showErrorDialog(message: string): void {
    Swal.fire(message, '', 'error');
  }

  onSubmit(form: NgForm) {
    if (form.valid) {

      const fechainicial = form.value.fechaInicio as Moment;
      const fechafinal = form.value.fechaFin as Moment;
      this.presenter.loadEgresosByDate(fechainicial.toDate().getTime(), fechafinal.toDate().getTime());
    }
  }
}
