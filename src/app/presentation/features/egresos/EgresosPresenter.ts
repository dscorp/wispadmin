import {EgresosMVP} from '../../../domain/mvpdefinition/EgresosMVP';
import {NetworkCallBack} from '../../../domain/entity/NetworkCallBack';
import {Egreso} from '../../../domain/entity/Egreso';
import {Injectable} from '@angular/core';

@Injectable()
export class EgresosPresenter implements EgresosMVP.Presenter {
  private view: EgresosMVP.View;


  constructor(private model: EgresosMVP.Model) {
  }

  setView(view: EgresosMVP.View): void {
    this.view = view;
  }


  loadEgresosByDate(startDate, endingDate): void {
    const parent = this;
    this.model.loadEgresosByDate(startDate, endingDate, new class implements NetworkCallBack<Egreso[]> {
      onComplete(result: Egreso[]) {
        parent.view.onloadEgresosByDateRangeReady(result);
      }

      onException(e: Error) {
        parent.view.showErrorDialog(e.message);
      }

    });
  }


}
