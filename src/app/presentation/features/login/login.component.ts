import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Usuario} from 'src/app/domain/entity/Usuario';
import Swal from 'sweetalert2';
import {LoginMVP} from '../../../domain/mvpdefinition/LoginMVP';
import {Router} from '@angular/router';
import {AuthService} from '../../../framework/services/auth.service';
import {DialogFactory} from '../../DialogFactory';
import {DialogTypes} from '../../../domain/entity/DialogTypes';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, LoginMVP.View {
  loginUsername: string;
  loginPassword: string;
  login: boolean = true;
  usuario = new Usuario();
  private form: NgForm;
  @ViewChild('saveBtn') saveBtn;

  constructor(
    private presenter: LoginMVP.Presenter,
    private router: Router,
    private authService: AuthService,
    private dialogFactory: DialogFactory
  ) {
  }

  registrar(form: NgForm) {
    this.form = form;
    if (form.valid) {
      const stringvalue = JSON.stringify(form.value).toLowerCase();
      this.presenter.saveUser(JSON.parse(stringvalue) as Usuario);
    } else {
      Swal.fire('Los datos son incorrectos', '', 'error');
    }

  }

  onSaveUserFails(user?: Usuario, message?: String) {
    Swal.fire('Ocurrió un error en el registro: ' + message, '', 'error');
  }

  onSaveUserSuccess(user: Usuario) {
    Swal.fire('Registro exitoso!', '<p>Espere a que su cuenta sea activada para poder iniciar sesión</p>', 'success');
    this.form.resetForm();
  }

  ngOnInit(): void {
    this.presenter.setView(this);
    if (this.authService.isAutenticated()) {
      this.router.navigate(['/admin/dashboard']);
    }
  }

  onSignInFail(user?: Usuario, message?: String) {
    Swal.fire('Asegurese de ingresar los datos correctos e intentelo de nuevo: ' + message, '', 'error');

  }

  onSignInSuccessfull(user: Usuario) {
    if (user.verified) {
      localStorage.setItem('name', user.name);
      localStorage.setItem('lastname', user.lastname);
      localStorage.setItem('user_id', user.id);
      localStorage.setItem('user_type', user.type.toString());
      this.authService.getUser();
      this.router.navigate(['admin/dashboard']);
      // Swal.fire(`Bienvenido : ${user?.name}`, '', 'success');
    } else {
      Swal.fire(`Hola, ${user?.name}`, 'Tu cuenta está pendiente de verificación, si tienes dudas contacta con soporte técnico', 'warning');
    }
  }

  signIn(signInForm: NgForm) {
    if (signInForm.valid) {
      const formobj = signInForm.value;
      this.presenter.signIn(formobj.username, formobj.password);
    } else {
      Swal.fire('Asegurese de ingresar las credenciales correctamente.', '', 'warning');
    }
  }

  onCredentialsDoesntMatch(message?: String) {
    Swal.fire(`Credenciales Incorrectas`, 'Asegurese de ingresar bien las credenciales y vuelva a intentarlo', 'error');
  }

  showLoadingDialog(showDialog: boolean) {
    if (showDialog) {
      this.dialogFactory.createDialog(DialogTypes.load);
    } else {
      this.dialogFactory.closeDialog();
    }
  }

  disableSaveButton(b: boolean) {
    this.saveBtn.nativeElement.disabled = b;
    console.log(this.saveBtn.nativeElement);
  }
}


