import {Injectable} from '@angular/core';
import {LoginMVP} from '../../../domain/mvpdefinition/LoginMVP';
import {Usuario} from '../../../domain/entity/Usuario';
import {NetworkCallBack} from '../../../domain/entity/NetworkCallBack';

@Injectable()
export class LoginPresenter implements LoginMVP.Presenter {
  private view: LoginMVP.View;

  constructor(private model: LoginMVP.Model) {
  }

  saveUser(user: Usuario,context=this) {
    console.log('sergio');
    context.view.showLoadingDialog(true)
    context.view.disableSaveButton(true)
    this.model.saveUser(user, new class implements NetworkCallBack<Usuario> {
      onComplete(result: Usuario) {
        context.view.disableSaveButton(false)
        context.view.showLoadingDialog(false)
        context.view.onSaveUserSuccess(result);
      }

      onException(e: Error) {
        context.view.disableSaveButton(false)
        context.view.showLoadingDialog(false)
        context.view.onSaveUserFails(undefined, e.message);
      }
    });
  }

  setView(view: LoginMVP.View) {
    this.view = view;
  }

  signIn(username: string, password: string) {

    const parent = this;
    parent.view.showLoadingDialog(true)
    this.model.signIn(username, password, new class implements NetworkCallBack<Usuario> {
      onComplete(result: Usuario) {
        parent.view.showLoadingDialog(false)
        if (result != null) {
          parent.view.onSignInSuccessfull(result);
        } else {
          parent.view.onCredentialsDoesntMatch();
        }
      }

      onException(e: Error) {
        parent.view.showLoadingDialog(false)
        parent.view.onSignInFail(undefined,
          `${e.message}`);
      }
    });

  }

}
