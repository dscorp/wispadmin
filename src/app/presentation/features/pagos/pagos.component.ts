import { Component, OnInit, ViewChild } from '@angular/core';

import { NgForm } from '@angular/forms';
import { PagosMVP } from '../../../domain/mvpdefinition/PagosMVP';
import { Pago } from 'src/app/domain/entity/Pago';
import Swal from 'sweetalert2';
import { Moment } from 'moment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import jsPDF from 'jspdf';
import { DatePipe } from '@angular/common';
import autoTable from 'jspdf-autotable';


@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.css'],
  providers: [DatePipe],
})
export class PagosComponent implements OnInit, PagosMVP.View {
  displayedColumns: string[] = ['nombreCliente', 'deuda', 'lugarCliente', 'direccionCliente', 'fecha'];
  dataSource: MatTableDataSource<Pago>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  fecheInicio1: any;
  fechaFin1: any;
  pagos: Pago[];
  porCobrar: number;
  cobrado: number;
  descontado: number;
  ingresoBruto: number;

  constructor(
    private presenter: PagosMVP.Presenter,
    public datepipe: DatePipe,
  ) {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onPagosByDateRangeReady(result: Pago[]): void {
    this.pagos = result;
    this.calcularMontoporCobrar();
    this.dataSource = new MatTableDataSource<Pago>(this.pagos);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }


  exportPDFDeudores() {
    const deudores = this.pagos.filter((pago: Pago) => pago.pagado == false);
    deudores.sort((a, b) => (((a.nombreCliente) as string) > ((b.nombreCliente) as string) ? 1 : -1));
    deudores.sort((a, b) => (((a.lugarCliente) as string) > ((b.lugarCliente) as string) ? 1 : -1));

    const doc = new jsPDF();

    const arr: any[] = [];
    deudores.forEach((pago) => {
      arr.push([
        pago.nombreCliente,
        pago.deuda,
        pago.lugarCliente,
        pago.direccionCliente,
        pago.telefonoCLiente,
        this.datepipe.transform((pago.fecha as any).toMillis()),
      ]);
    });


    doc.text('CLIENTES DEUDORES:\b\b' + this.datepipe.transform(this.fecheInicio1) + ' - ' + this.datepipe.transform(this.fechaFin1), 15, 15);

    autoTable(doc, {
      head: [['CLIENTE', 'DEUDA', 'LUGAR', 'DIRECCION', 'TLF', 'FECHA']],
      body: arr,
      margin: { top: 25 }
    });

    doc.save(`ClientesDeudores ${this.datepipe.transform(this.fecheInicio1)} - ${this.datepipe.transform(this.fechaFin1)}.pdf`);

  }





  exportPDFResumenCobradores() {
    const pagados = this.pagos.filter((pago: Pago) => pago.pagado == true);
    // pagados.sort((a, b) => (((a.nombreCliente) as string) > ((b.nombreCliente) as string) ? 1 : -1));
    // pagados.sort((a, b) => (((a.lugarCliente) as string) > ((b.lugarCliente) as string) ? 1 : -1));

    const groupedMap = pagados.reduce(
      (entryMap, e) => entryMap.set(e.encargado, [...entryMap.get(e.encargado) || [], e]),
      new Map()
    );

    const finalArray = []


    groupedMap.forEach((value, key: string) => {
      let totalNetoRecaudado:number = 0
      let totalBrutoCobrado:number = 0
      let totalDescuentos:number = 0
      value.forEach(element => {
        totalNetoRecaudado = parseFloat(element.montoAbonado + "") + totalNetoRecaudado;
        totalBrutoCobrado = parseFloat(element.deuda + "") + totalBrutoCobrado;
        totalDescuentos = parseFloat(element.descuento + "") + totalDescuentos;
      });

      finalArray.push({
        "nombrepersona": key,
        "totalneto": totalNetoRecaudado.toLocaleString('es-PE', { style: 'currency', currency: 'PEN' }),
        "totalbruto": totalBrutoCobrado.toLocaleString('es-PE', { style: 'currency', currency: 'PEN' }),
        "totaldescuento": totalDescuentos.toLocaleString('es-PE', { style: 'currency', currency: 'PEN' })
      })

    })

    console.log(finalArray)
       const doc = new jsPDF();
       const arr: any[] = [];
       finalArray.forEach((element) => {
         arr.push([
           element.nombrepersona.toUpperCase(),
           element.totalbruto,
           element.totalneto,
           element.totaldescuento,
         ]);
       });

     doc.text('RESUMENT DE COBROS:\b\b del mes ' + this.datepipe.transform(this.fecheInicio1) +' generado el '+this.datepipe.transform(new Date()), 15, 15);

     autoTable(doc, {
       head: [['ENCARGADO', 'TOTAL BRUTO', 'TOTAL NETO', 'TOTAL DESCUENTOS']],
       body: arr,
       margin: {top: 25}
     });

     doc.save(`ResumenCobradores del${this.datepipe.transform(this.fecheInicio1)} - ${this.datepipe.transform(this.fechaFin1)}.pdf`);

  }


  exportPDFTodos() {

    const data = this.pagos;

    data.sort((a, b) => (((a.nombreCliente) as string) > ((b.nombreCliente) as string) ? 1 : -1));
    data.sort((a, b) => (((a.lugarCliente) as string) > ((b.lugarCliente) as string) ? 1 : -1));

    const doc = new jsPDF();

    const arr: any[] = [];
    data.forEach((pago) => {
      arr.push([
        pago.nombreCliente,
        pago.deuda,
        pago.lugarCliente,
        pago.direccionCliente,
        this.datepipe.transform((pago.fecha as any).toMillis()),
      ]);
    });


    doc.text('LISTA DE CLIENTES:\b\b' + this.datepipe.transform(this.fecheInicio1) + ' - ' + this.datepipe.transform(this.fechaFin1), 15, 15);

    autoTable(doc, {
      head: [['CLIENTE', 'DEUDA', 'LUGAR', 'DIRECCION', 'FECHA']],
      body: arr,
      margin: { top: 25 }
    });

    doc.save(`Clientes ${this.datepipe.transform(this.fecheInicio1)} - ${this.datepipe.transform(this.fechaFin1)}.pdf`);

  }

  calcularMontoporCobrar() {
    let porCobrar: number = 0;
    let cobrado: number = 0;
    let descontado: number = 0;
    let ingresoBruto: number = 0;

    this.pagos.forEach((value: Pago) => {

      ingresoBruto = parseFloat(value.deuda + "") + ingresoBruto;

      if (!value.pagado) {
        porCobrar += parseFloat(value.deuda + "");
      } else {
        cobrado += parseFloat(value.montoAbonado + "");
        descontado += parseFloat(value.descuento + "");
      }
    });
    this.porCobrar = porCobrar;
    this.cobrado = cobrado;
    this.descontado = descontado;
    this.ingresoBruto = ingresoBruto;
  }

  showErrorDialog(message: string): void {
    Swal.fire(message, '', 'error');
  }


  ngOnInit(): void {
    this.presenter.setView(this);
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      console.log(form);
      const fechainicial = form.value.fechaInicio as Moment;
      const fechafinal = form.value.fechaFin as Moment;
      this.presenter.getPagosByDateRange(fechainicial.toDate(), fechafinal.toDate());
    }
  }


}
