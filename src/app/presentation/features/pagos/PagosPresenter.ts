import {PagosMVP} from '../../../domain/mvpdefinition/PagosMVP';
import {Injectable} from '@angular/core';
import {Pago} from '../../../domain/entity/Pago';
import {NetworkCallBack} from '../../../domain/entity/NetworkCallBack';

@Injectable()
export class PagosPresenter implements PagosMVP.Presenter {

  constructor(private model: PagosMVP.Model) {
  }

  private view: PagosMVP.View;

  getPagosByDateRange(fecha1: any, fecha2: any) {
    const parent = this
    this.model.getByDateRange(fecha1, fecha2, new class implements NetworkCallBack<Pago[]> {
      onComplete(result: Pago[]) {
        parent.view.onPagosByDateRangeReady(result)
      }

      onException(e: Error) {
        parent.view.showErrorDialog(`No se pudo filtrar los pagos por rango de fechas ERROR: ${e.message}`)
      }
    });
  }

  setView(view: PagosMVP.View) {
    this.view = view;
  }


}
