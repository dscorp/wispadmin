import {SuscripcionesMVP} from '../../../domain/mvpdefinition/SuscripcionesMVP';
import {Suscripcion} from '../../../domain/entity/Suscripcion';
import {NetworkCallBack} from '../../../domain/entity/NetworkCallBack';
import {Injectable} from '@angular/core';

@Injectable()
export class SuscripcionesPresenter implements SuscripcionesMVP.Presenter {
  private view: SuscripcionesMVP.View;

  constructor(
    private model: SuscripcionesMVP.Model
  ) {
  }

  setView(view: SuscripcionesMVP.View) {
    this.view = view;
  }

  saveSuscripcion(suscripcion: Suscripcion, context = this) {
    context.view.showLoadingDialog(true);
    this.model.saveSuscripcion(suscripcion, new class implements NetworkCallBack<Suscripcion> {
      onComplete(result: Suscripcion) {
        context.view.showLoadingDialog(false);
        context.view.showInfoDialog('La suscripcion fue registrada exitosamente');
      }

      onException(e: Error) {
        context.view.showLoadingDialog(false);
        context.view.showErrorDialog('No se pudo registrar la suscripcion ERROR:' + e.message);
      }
    });
  }

  getAllSuscripcion(context=this) {
    context.view.showLoadingDialog(true)
    this.model.getAllSuscripcion(new class implements NetworkCallBack<Suscripcion[]> {
      onComplete(result: Suscripcion[]) {
        context.view.showLoadingDialog(false)
        context.view.onSuscripcionesListReady(result);
      }

      onException(e: Error) {
        context.view.showLoadingDialog(false)
        context.view.showErrorDialog('No se pudo traer las suscripciones de la base de datos : error:' + e.message);
      }
    });
  }

  cortarServicio(suscripcion: Suscripcion,context=this) {
    context.view.showLoadingDialog(true)
    suscripcion.servicioSuspendido = true;
    this.model.updateSuscripcion(suscripcion, new class implements NetworkCallBack<boolean> {
      onComplete(result: boolean) {
        context.view.showLoadingDialog(false)
        context.view.showInfoDialog(`El servicio de ${suscripcion.nombreCompleto} fue suspendido correctamente!`);
      }

      onException(e: Error) {
        context.view.showLoadingDialog(false)
        context.view.showErrorDialog(`Ocurrio un error al suspender el serviciode ${suscripcion.nombreCompleto} error: ${e.message}`);
      }
    });
  }

  reactivarServicio(suscripcion: Suscripcion, context=this) {
    context.view.showLoadingDialog(true)
    suscripcion.servicioSuspendido = false;
    this.model.updateSuscripcion(suscripcion, new class implements NetworkCallBack<boolean> {
      onComplete(result: boolean) {
        context.view.showLoadingDialog(false)
        context.view.showInfoDialog(`El servicio de ${suscripcion.nombreCompleto} fue reactivado correctamente!`);
      }

      onException(e: Error) {
        context.view.showLoadingDialog(false)
        context.view.showErrorDialog(`Ocurrio un error al reactivar el serviciode ${suscripcion.nombreCompleto} error: ${e.message}`);
      }
    });
  }

  updateSuscription(suscripcion: Suscripcion,context=this) {
    context.view.showLoadingDialog(true)
    this.model.updateSuscripcion(suscripcion, new class implements NetworkCallBack<boolean> {
      onComplete(result: boolean) {
        context.view.showLoadingDialog(false)
        context.view.showInfoDialog(`Los datos de la suscripcion fueron actualizados correctamente`);
      }

      onException(e: Error) {
        context.view.showLoadingDialog(false)
        context.view.showErrorDialog(`No se pudo actualizar los datos de la suscripcion ERROR: ${e.message}`);
      }
    });
  }

  getSuscripcionesConCorte(context=this) {
    context.view.showLoadingDialog(true)
    this.model.getSuscripcionesConCorte(new class implements NetworkCallBack<Suscripcion[]> {
      onComplete(result: Suscripcion[]) {
        context.view.showLoadingDialog(false)
        context.view.onSuscripcionesConCorteReady(result);
      }

      onException(e: Error) {
        context.view.showLoadingDialog(false)
        context.view.showErrorDialog(`No se pudo consultar las suscripciones con corte ERROR: ${e.message}`);
      }
    });
  }
}
