import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SaveSuscripcionDialogComponent} from '../../save-suscripcion-dialog/save-suscripcion-dialog.component';
import {Pago} from '../../../../../domain/entity/Pago';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-save-pago-dialog',
  templateUrl: './save-pago-dlg.component.html',
  styleUrls: ['./save-pago-dlg.component.css']
})
export class SavePagoDlgComponent implements OnInit {
  formPago: FormGroup;
  private pago: Pago;

  constructor(
    public dialogRef: MatDialogRef<SaveSuscripcionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
  ) {
  }

  ngOnInit(): void {

    this.pago = this.data;
    this.formPago = this.fb.group({
      id: [this.pago.id, [Validators.required]],
      descuento: [this.pago.descuento, [Validators.required, Validators.max(this.pago.deuda), Validators.min(0)]],
      motivo: [this.pago.motivo, [Validators.required]],
      montoAbonado: [this.pago.deuda, [Validators.required]],
      pagado: [true, [Validators.required]],
      deuda: [{value: this.pago.deuda, disabled: true}, [Validators.required]],
      fecha: [this.pago.fecha, [Validators.required]],
      idSuscripcion: [this.pago.idSuscripcion, [Validators.required]],
      nombreCliente: [this.pago.nombreCliente, [Validators.required]],
      telefonoCLiente: [this.pago.telefonoCLiente, [Validators.required]],
      direccionCliente: [this.pago.direccionCliente, [Validators.required]],
      metodo: [this.pago.metodo, [Validators.required]]
    });

  }


  savePago() {
    const formval = this.formPago.value as Pago;

    formval.montoAbonado -= formval.descuento;
    this.dialogRef.close(formval);
  }

}
