import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePagoDlgComponent } from './update-pago-dlg.component';

describe('UpdatePagoDlgComponent', () => {
  let component: UpdatePagoDlgComponent;
  let fixture: ComponentFixture<UpdatePagoDlgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdatePagoDlgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePagoDlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
