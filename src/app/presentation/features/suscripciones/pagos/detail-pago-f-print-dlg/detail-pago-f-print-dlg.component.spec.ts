import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPagoFPrintDlgComponent } from './detail-pago-f-print-dlg.component';

describe('DetailPagoFPrintDlgComponent', () => {
  let component: DetailPagoFPrintDlgComponent;
  let fixture: ComponentFixture<DetailPagoFPrintDlgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailPagoFPrintDlgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPagoFPrintDlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
