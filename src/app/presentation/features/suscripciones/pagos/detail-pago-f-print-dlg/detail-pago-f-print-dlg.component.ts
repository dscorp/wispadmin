import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SaveSuscripcionDialogComponent} from '../../save-suscripcion-dialog/save-suscripcion-dialog.component';
import {Pago} from '../../../../../domain/entity/Pago';
import {AuthService} from '../../../../../framework/services/auth.service';
import * as htmlToImage from 'html-to-image';

@Component({
  selector: 'app-detail-pago-f-print-dlg',
  templateUrl: './detail-pago-f-print-dlg.component.html',
  styleUrls: ['./detail-pago-f-print-dlg.component.css']
})
export class DetailPagoFPrintDlgComponent implements OnInit {
  pago: Pago;
  suscripcion: any;


  constructor(
    public dialogRef: MatDialogRef<SaveSuscripcionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    console.log("data");

    console.log(this.data);

    this.pago = this.data.pago
    this.suscripcion=this.data.suscripcion
    // this.suscripcion = this.data.suscripcion;
    // console.log(this.suscripcion);
    // this.formPago = this.fb.group({
    //   deuda: [this.suscripcion.precioPlan, [Validators.required]],
    //   fecha: [new Date(), [Validators.required]],
    //   descuento: [0, [Validators.required]],
    //   motivo: ['-', [Validators.required]],
    //   montoAbonado: [0, [Validators.required]],
    //   pagado: [false, [Validators.required]],
    //   idSuscripcion: [this.suscripcion.id, [Validators.required]],
    //   nombreCliente: [this.suscripcion.nombreCompleto, [Validators.required]],
    //   telefonoCLiente: [this.suscripcion.telefono, [Validators.required]],
    //   lugarCliente: [this.suscripcion.lugar, [Validators.required]],
    //   direccionCliente: [this.suscripcion.direccion, [Validators.required]],
    // });

  }


  savePago() {
    this.dialogRef.close();
  }


  print() {
    var node: any = document.getElementById('image-section');
    htmlToImage.toPng(node, {canvasWidth: 350})
      .then(function(dataUrl) {
        var win = window.open('');
        win.document.write('<img src="' + dataUrl + '" onload="window.print();window.close()" />');
        win.focus();
        // var img = new Image();
        // img.src = dataUrl;
        // document.body.appendChild(img);
      })
      .catch(function(error) {
        console.error('oops, something went wrong!', error);
      });
  }


  async sendEmail() {

  }
}
