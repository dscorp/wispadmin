import {PagosBySuscripcionMVP} from '../../../../../domain/mvpdefinition/PagosBySuscripcionMVP';
import {Suscripcion} from '../../../../../domain/entity/Suscripcion';
import {NetworkCallBack} from '../../../../../domain/entity/NetworkCallBack';
import {Pago} from '../../../../../domain/entity/Pago';
import {Injectable} from '@angular/core';
import {Subscription} from 'rxjs';

@Injectable()
export class PagosBySyscripcionPresenter implements PagosBySuscripcionMVP.Presenter {
  private view: PagosBySuscripcionMVP.View;

  private subscriptionPagosBySuscripcion: Subscription;

  constructor(
    private model: PagosBySuscripcionMVP.Model,
  ) {
  }

  getPagosBySuscripcion(suscripcion: Suscripcion, context = this) {
    context.view.showLoading(true);
    this.subscriptionPagosBySuscripcion = this.model.getPagosBySuscripcion(suscripcion, new class implements NetworkCallBack<Pago[]> {
      onComplete(result: Pago[]) {
        console.log('pagos',result);
        context.view.showLoading(false);
        context.view.onPagosBySuscripcionReady(result);
      }

      onException(e: Error) {
        console.log('error',e);
        context.view.showLoading(false);
        context.view.showErrorDialog(e.message);
      }
    })
    ;
  }

  savePagoBySuscripcion(pago: Pago, context = this) {
    context.view.showLoading(true);
    this.model.savePago(pago, new class implements NetworkCallBack<Pago> {
      onComplete(result: Pago) {
        context.view.showLoading(false);
        context.view.showInfoDialog(`El pago fué registrado exitosamente`);
      }

      onException(e: Error) {
        context.view.showLoading(false);
        context.view.showErrorDialog(`Ocurrio un error al registrar el pago : ${e.message}`);
      }
    });
  }

  setView(view: PagosBySuscripcionMVP.View) {
    this.view = view;
  }

  unsuscribePagosBySuscription() {
    if (this.subscriptionPagosBySuscripcion != null) {
      this.subscriptionPagosBySuscripcion.unsubscribe();
    }
  }

  updatePago(pago: Pago, context = this) {
    context.view.showLoading(true);
    context.view.disableBtnSavePago(true)
    this.model.updatePago(pago, new class implements NetworkCallBack<Pago> {
      onComplete(result: Pago) {
        context.view.disableBtnSavePago(false)
        context.view.showLoading(false);
        context.view.showInfoDialog('El pago fue realizado exitosamente!');
      }

      onException(e: Error) {
        context.view.disableBtnSavePago(false)
        context.view.showLoading(false);
        context.view.showErrorDialog(`Ocurrio un error al registrar el pago Error: ${e.message}`);
      }
    });

  }

}
