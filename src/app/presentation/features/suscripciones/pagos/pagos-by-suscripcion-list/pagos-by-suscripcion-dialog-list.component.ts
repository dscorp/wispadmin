import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Suscripcion} from '../../../../../domain/entity/Suscripcion';
import {ActivatedRoute} from '@angular/router';
import {PagosBySuscripcionMVP} from '../../../../../domain/mvpdefinition/PagosBySuscripcionMVP';
import {Pago} from '../../../../../domain/entity/Pago';
import Swal from 'sweetalert2';
import {SavePagoDlgComponent} from '../save-pago-dlg/save-pago-dlg.component';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AuthService} from '../../../../../framework/services/auth.service';
import {UpdatePagoDlgComponent} from '../update-pago-dlg/update-pago-dlg.component';
import {DialogUtil} from '../../../../dialog-util.service';
import {DialogCallback} from '../../../../../domain/entity/DialogCallback';
import {DetailPagoFPrintDlgComponent} from '../detail-pago-f-print-dlg/detail-pago-f-print-dlg.component';
import {DialogFactory} from '../../../../DialogFactory';
import {DialogTypes} from '../../../../../domain/entity/DialogTypes';
import {Usuario} from '../../../../../domain/entity/Usuario';

@Component({
  selector: 'app-pagos-by-suscripcion-list',
  templateUrl: './pagos-by-suscripcion-dialog-list.component.html',
  styleUrls: ['./pagos-by-suscripcion-dialog-list.component.css']
})

export class PagosBySuscripcionDialogListComponent implements OnInit, OnDestroy, PagosBySuscripcionMVP.View {

  dataSource: MatTableDataSource<Suscripcion>;
  displayedColumns: string[] = ['fecha', 'deuda', 'montoAbonado', 'descuento', 'motivo', 'buttons'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  private suscripcion: Suscripcion;
  private operator: Usuario;

  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PagosBySuscripcionDialogListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private presenter: PagosBySuscripcionMVP.Presenter,
    private authService: AuthService,
    private dialogUtil: DialogUtil,
    private dialogFactory: DialogFactory,
  ) {
    this.operator= authService.getUser()
  }

  onPagosBySuscripcionReady(pagos: any[]) {
    this.dataSource = new MatTableDataSource<Suscripcion>(pagos);
    this.dataSource.sort = this.sort;

  }

  ngOnInit(): void {
    this.presenter.setView(this);
    //RECIBE UN OBJETO SUSCRIPCION POR PARAMETRO DEL DIALOGO
    this.suscripcion = this.data;
    this.presenter.getPagosBySuscripcion(this.suscripcion);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openUpdatePagoDialog(pago: Pago, context = this) {
    this.dialogUtil.openDialog(UpdatePagoDlgComponent, pago, '350px', new class implements DialogCallback<any> {
      onDialogClosed(result: Pago) {

        result.modificadopor=`${context.operator.name} ${context.operator.lastname}`
        context.presenter.updatePago(result);
        // context.dialogRef.close();
      }
    });
  }

  openSavePagoDialog(pago: Pago, context = this) {
    this.dialogUtil.openDialog(SavePagoDlgComponent, pago, '350px', new class implements DialogCallback<any> {
      onDialogClosed(result: Pago) {
       result.encargado=`${context.operator.name} ${context.operator.lastname}`
        context.presenter.updatePago(result);
        // context.dialogRef.close();
      }
    });
  }

  savePago(pago: Pago) {
    this.presenter.savePagoBySuscripcion(pago);
  }

  showErrorDialog(message: string) {
    Swal.fire(message, '', 'error');
  }

  showInfoDialog(message: string) {
    Swal.fire(message, '', 'success');
  }

  ngOnDestroy(): void {
    this.presenter.unsuscribePagosBySuscription();
  }

  imprimirComprobante(pago: Pago, context = this) {
    context.dialogUtil.openDialog(DetailPagoFPrintDlgComponent,
      {
        "pago":pago,
       "suscripcion":this.suscripcion
     }
    , '400px', new class implements DialogCallback<any> {
      onDialogClosed(result: any) {
        context.dialogRef.close();
      }
    });
  }

  showLoading(show: boolean): void {
    if (show) {
      this.dialogFactory.createDialog(DialogTypes.load);
    } else {
      this.dialogFactory.closeDialog();
    }
  }

  disableBtnSavePago(disable: boolean) {
    // this.savepagobtn.disabled = disable;
  }
}
