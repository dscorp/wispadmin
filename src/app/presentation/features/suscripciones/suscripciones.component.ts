import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {SaveSuscripcionDialogComponent} from './save-suscripcion-dialog/save-suscripcion-dialog.component';
import {Suscripcion} from '../../../domain/entity/Suscripcion';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {SuscripcionesMVP} from '../../../domain/mvpdefinition/SuscripcionesMVP';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {UpdateSuscripcionDialogComponent} from './update-suscripcion-dialog/update-suscripcion-dialog.component';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import autoTable from 'jspdf-autotable';
import {AngularFirestore} from '@angular/fire/firestore';
import {PagosBySuscripcionDialogListComponent} from './pagos/pagos-by-suscripcion-list/pagos-by-suscripcion-dialog-list.component';
import {DialogFactory} from '../../DialogFactory';
import {DialogTypes} from '../../../domain/entity/DialogTypes';


@Component({
  selector: 'app-suscripciones',
  templateUrl: './suscripciones.component.html',
  styleUrls: ['./suscripciones.component.css']
})
export class SuscripcionesComponent implements OnInit, SuscripcionesMVP.View {
  displayedColumns: string[] = ['codigo', 'nombreCompleto', 'direccion', 'lugar', 'servicioSuspendido', 'menu'];
  dataSource: MatTableDataSource<Suscripcion>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private presenter: SuscripcionesMVP.Presenter,
    private router: Router,
    private firestore: AngularFirestore,
    private dialogFactory: DialogFactory
  ) {
    this.presenter.setView(this);
  }

  onSuscripcionesListReady(result: Suscripcion[]): void {
    this.dataSource = new MatTableDataSource<Suscripcion>(result);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  openDialogRegistroSuscripcion(): void {
    const dialogRef = this.dialog.open(SaveSuscripcionDialogComponent, {
      width: '350px',
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result != undefined) {
        this.presenter.saveSuscripcion(result);
      }
    });
  }


  importDataFromJson() {
    var data: [] = require('./suscripciones.json');

    const suscripcionesForPush = [];


    data.forEach((cliente: any) => {
      const id = this.firestore.createId();
      const suscripcion = new Suscripcion(
        id,
        cliente.nombreCompleto,
        '-',
        '-',
        cliente.lugar,
        cliente.nombrePlan,
        cliente.nombrePlan,
        cliente.velocidadSubida,
        cliente.velocidadBajada,
        new Date(),
        false,
        false,
      );

      const data = {...suscripcion};
      this.firestore.collection('Suscripciones').doc(suscripcion.id).set({...suscripcion});
    });


  }


  ngOnInit(): void {
    this.getAllSuscripciones();
  }

  getAllSuscripciones() {
    this.presenter.getAllSuscripcion();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }

  cortarServicio(suscripcion: Suscripcion) {
    Swal.fire({
      title: `Está seguro que desea cortar el servicio de ${suscripcion.nombreCompleto} ?`,
      icon: 'warning',
      showDenyButton: true,
      confirmButtonText: `Si, Cortar`,
      denyButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.presenter.cortarServicio(suscripcion);
      }
    });
  }

  openDialogactualizarInformacion(suscripcion: Suscripcion) {
    const dialogRef = this.dialog.open(UpdateSuscripcionDialogComponent, {
      width: '350px',
      data: {suscripcion: suscripcion}
    });
    dialogRef.afterClosed().subscribe((suscripcion: Suscripcion) => {
      console.log('suscripcion', suscripcion);
      if (suscripcion != undefined) {
        this.presenter.updateSuscription(suscripcion);
      }
    });
  }

  openViewPagosDeSuscripcion(suscripcion: Suscripcion) {

    const dialogRef = this.dialog.open(PagosBySuscripcionDialogListComponent, {
      width: '860px',
      data: suscripcion,
    });

    dialogRef.afterClosed().subscribe((suscripcion: any) => {

      }
    );

    // this.router.navigate(['/listaPagosSuscripcion'], {queryParams: {suscripcion: JSON.stringify(suscripcion)}});
  }

  reactivarServicio(suscripcion: Suscripcion) {
    this.presenter.reactivarServicio(suscripcion);
  }


  showInfoDialog(message: string): void {
    Swal.fire(message, '', 'success');
  }

  showErrorDialog(message: string): void {
    Swal.fire(message, '', 'error');
  }

  getSuscripcionesConCorte() {
    this.presenter.getSuscripcionesConCorte();
  }

  onSuscripcionesConCorteReady(suscripciones: Suscripcion[]) {
    const doc = new jsPDF();
    const arr = [];
    suscripciones.forEach(suscripcion => {
      arr.push([
        suscripcion.nombreCompleto,
        suscripcion.lugar + ' - ' + suscripcion.direccion,
        suscripcion.telefono,
        suscripcion.nombrePlan,
        suscripcion.precioPlan,
      ]);
    });

    doc.text('CLIENTES CON ORDEN DE CORTE DE SERVICIO', 15, 15);

    autoTable(doc, {
      head: [['CLIENTE', 'DIRECCIÓN', 'TELEÉFONO', 'PLAN', 'PRECIO PLAN']],
      body: arr,
      margin: {top: 25}
    });

    doc.save('SuscripcionesConCorte.pdf');
  }

  exportPDFAllSuscripciones() {
    const doc = new jsPDF();

    const arr = [];
    this.dataSource.data.forEach(suscripcion => {
      arr.push([
        suscripcion.nombreCompleto,
        suscripcion.lugar + ' - ' + suscripcion.direccion,
        suscripcion.telefono,
        suscripcion.nombrePlan,
        suscripcion.precioPlan,
      ]);
    });

    doc.text('LISTA DE TODOS LOS CLIENTES', 15, 15);

    autoTable(doc, {
      head: [['CLIENTE', 'DIRECCIÓN', 'TELEÉFONO', 'PLAN', 'PRECIO PLAN']],
      body: arr,
      margin: {top: 25}
    });

    doc.save('SuscripcionesConCorte.pdf');
  }

  showLoadingDialog(show: boolean) {
    if (show) {
      this.dialogFactory.createDialog(DialogTypes.load);
    } else {
      this.dialogFactory.closeDialog();
    }

  }
}
