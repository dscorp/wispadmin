import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PlanRepository} from '../../../../domain/repository/PlanRepository';
import {Plan} from '../../../../domain/entity/Plan';
import {NetworkCallBack} from '../../../../domain/entity/NetworkCallBack';

@Component({
  selector: 'app-save-suscripcion-dialog',
  templateUrl: './save-suscripcion-dialog.component.html',
  styleUrls: ['./save-suscripcion-dialog.component.css']
})
export class SaveSuscripcionDialogComponent implements OnInit {
  suscripcionForm = this.fb.group({
    nombreCompleto: ['', [Validators.required, Validators.minLength(4)]],
    telefono: ['', [Validators.required, Validators.maxLength(9), Validators.minLength(9)]],
    direccion: ['', [Validators.required, Validators.minLength(4)]],
    lugar: ['', [Validators.required]],
    nombrePlan: ['', [Validators.required]],
    precioPlan: [{value: '', disabled: true}, [Validators.required]],
    velocidadSubida: [{value: '', disabled: true}, [Validators.required]],
    velocidadBajada: [{value: '', disabled: true}, [Validators.required]],
    fechaSuscripcion: [, [Validators.required]],
    servicioSuspendido: [false, [Validators.required]],
    isNew: [true, [Validators.required]],
  });
  planes: Plan[] = [];

  constructor(
    public dialogRef: MatDialogRef<SaveSuscripcionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private planRepository: PlanRepository,
  ) {
  }

  getAllPlanes() {
    const parent = this;
    this.planRepository.getAll(new class implements NetworkCallBack<Plan[]> {
      onComplete(result: Plan[]) {
        parent.planes = result;
      }

      onException(e: Error) {
        console.error(e.message);
      }
    });
  }

  ngOnInit(): void {
    this.getAllPlanes();
  }

  saveSuscripcion() {
    const suscripcion = this.suscripcionForm.getRawValue();
    this.dialogRef.close(suscripcion);
  }

}
