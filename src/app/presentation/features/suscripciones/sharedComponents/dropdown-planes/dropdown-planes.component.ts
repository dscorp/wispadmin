import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Plan} from '../../../../../domain/entity/Plan';

@Component({
  selector: 'app-dropdown-planes',
  templateUrl: './dropdown-planes.component.html',
  styleUrls: ['./dropdown-planes.component.css']
})
export class DropdownPlanesComponent {

  @Input() suscripcionForm: FormGroup;

  @Input() planes: Plan[];

  selectedPlan: Plan;

  constructor() {

  }

  setPlan() {
    this.suscripcionForm.get('nombrePlan').setValue(this.selectedPlan.nombre);
    this.suscripcionForm.get('precioPlan').setValue(this.selectedPlan.precio);
    this.suscripcionForm.get('velocidadSubida').setValue(this.selectedPlan.velocidadSubida);
    this.suscripcionForm.get('velocidadBajada').setValue(this.selectedPlan.velocidadBajada);
  }
}
