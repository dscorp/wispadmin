import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LugarRepository} from '../../../../../domain/repository/LugarRepository';
import {NetworkCallBack} from '../../../../../domain/entity/NetworkCallBack';
import {Lugar} from '../../../../../domain/entity/Lugar';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-dropdown-lugares',
  templateUrl: './dropdown-lugares.component.html',
  styleUrls: ['./dropdown-lugares.component.css']
})
export class DropdownLugaresComponent implements OnInit {

  @Input() formControl: FormControl;


  lugares: Lugar[];
  selected: any;


  constructor(private lugarRepository: LugarRepository,) {
    this.getLugares();
  }



  ngOnInit(): void {
    this.selected=this.formControl.value.toLowerCase()

  }


  getLugares() {
    const parent = this;
    this.lugarRepository.getAll(new class implements NetworkCallBack<Lugar[]> {
      onComplete(result: Lugar[]) {
        parent.lugares = result;
      }

      onException(e: Error) {
        console.error(e.message);
      }
    });
  }
}
