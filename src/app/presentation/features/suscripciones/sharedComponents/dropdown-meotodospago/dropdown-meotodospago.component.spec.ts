import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownMeotodospagoComponent } from './dropdown-meotodospago.component';

describe('DropdownMeotodospagoComponent', () => {
  let component: DropdownMeotodospagoComponent;
  let fixture: ComponentFixture<DropdownMeotodospagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DropdownMeotodospagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownMeotodospagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
