import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MetodosPago} from '../../../../../domain/entity/MetodosPago';

@Component({
  selector: 'app-dropdown-meotodospago',
  templateUrl: './dropdown-meotodospago.component.html',
  styleUrls: ['./dropdown-meotodospago.component.css']
})
export class DropdownMeotodospagoComponent implements OnInit {
  @Input() formControl: FormControl;


  metodosPago=[];
  selected: any;


  constructor() {
    this.getMetodosPago();
  }


  ngOnInit(): void {
    // this.selected = this.formControl.value.toLowerCase();
  }


  getMetodosPago() {
    const enumarray = Object.values(MetodosPago)
    const mitad = enumarray.length / 2;
    this.metodosPago=enumarray.slice(0,mitad);
    console.log(this.metodosPago);
  }
}
