import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Suscripcion} from '../../../../domain/entity/Suscripcion';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PlanRepository} from '../../../../domain/repository/PlanRepository';
import {Plan} from '../../../../domain/entity/Plan';
import {NetworkCallBack} from '../../../../domain/entity/NetworkCallBack';
import {Moment} from 'moment';

@Component({
  selector: 'app-save-suscripcion-dialog',
  templateUrl: './update-suscripcion-dialog.component.html',
  styleUrls: ['./update-suscripcion-dialog.component.css']
})
export class UpdateSuscripcionDialogComponent implements OnInit {

  private suscripcion: Suscripcion;
  suscripcionForm: FormGroup;
  planes: Plan[];


  constructor(
    public dialogRef: MatDialogRef<UpdateSuscripcionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private planRepository: PlanRepository
  ) {
    this.getAllPlanes();
  }


  ngOnInit(): void {

    this.suscripcion = this.data.suscripcion;

    this.suscripcionForm = this.fb.group({
      id: [this.suscripcion.id, [Validators.required]],
      nombreCompleto: [this.suscripcion.nombreCompleto, [Validators.required, Validators.minLength(4)]],
      telefono: [this.suscripcion.telefono, [Validators.required, Validators.maxLength(9), Validators.minLength(9)]],
      direccion: [this.suscripcion.direccion, [Validators.required, Validators.minLength(4)]],
      lugar: [this.suscripcion.lugar, [Validators.required]],
      nombrePlan: [this.suscripcion.nombrePlan, [Validators.required]],
      precioPlan: [{value: this.suscripcion.precioPlan, disabled: true}, [Validators.required]],
      velocidadSubida: [{value: this.suscripcion.velocidadSubida, disabled: true}, [Validators.required]],
      velocidadBajada: [{value: this.suscripcion.velocidadBajada, disabled: true}, [Validators.required]],
      fechaSuscripcion: [{value: this.suscripcion.fechaSuscripcion, disabled: true}, [Validators.required]],
      servicioSuspendido: [this.suscripcion.servicioSuspendido, [Validators.required]],
      isNew: [true, [Validators.required]],
    });

  }

  getAllPlanes() {
    const parent = this;
    this.planRepository.getAll(new class implements NetworkCallBack<Plan[]> {

      onComplete(result: Plan[]) {
        parent.planes = result;
      }

      onException(e: Error) {
        console.error(e.message);
      }
    });
  }


  getfechaUI(): Date {
    const moment = this.suscripcionForm.get('fechaSuscripcion').value as Moment;
    return moment.toDate()
      ;
  }

  updateSuscripcion() {
    this.dialogRef.close(this.suscripcionForm.getRawValue() as Suscripcion);
  }
}
