import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../framework/services/auth.service';
import {Usuario} from '../../../domain/entity/Usuario';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  private logedUser: Usuario;
   user_label: string;

  constructor(private router:Router, private authService:AuthService) { }

  ngOnInit(): void {
    this.logedUser =this.authService.getUser()
    this.user_label = `${this.logedUser.name.toUpperCase()} ${this.logedUser.lastname.toUpperCase()}`
  }

  logOut() {
    localStorage.clear();
    this.router.navigate([''])
  }
}
