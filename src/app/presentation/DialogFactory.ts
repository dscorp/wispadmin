import Swal from 'sweetalert2';
import {Injectable} from '@angular/core';
import {DialogTypes} from '../domain/entity/DialogTypes';


@Injectable()
export class DialogFactory {
   dialog: Promise<any> = null;
  createDialog(dialogType: DialogTypes,title?:string, message?:string): Promise<any> {
    switch (dialogType) {

      case DialogTypes.load:
        // this.dialog = Swal.fire({
        //   title: 'Cargando...',
        //   html: 'Espere por favor...',
        //   allowEscapeKey: false,
        //   allowOutsideClick: false,
        //   didOpen: () => {
        //     Swal.showLoading();
        //   }
        // });
        break;

      case DialogTypes.message:
        break;

      case DialogTypes.confirmation:
        break;

    }
    return this.dialog;
  }


  closeDialog() {
    // Swal.close();
  }
}
