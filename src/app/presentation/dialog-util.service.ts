import {DialogCallback} from '../domain/entity/DialogCallback';
import {MatDialog} from '@angular/material/dialog';
import {SavePagoDlgComponent} from './features/suscripciones/pagos/save-pago-dlg/save-pago-dlg.component';
import {Injectable} from '@angular/core';
import {ComponentType} from '@angular/cdk/portal';

@Injectable()
export class DialogUtil {

  constructor(private dialog: MatDialog) {
  }

   openDialog(dialogComponent,data, widthInPx: string, callback: DialogCallback<any>) {
    const dialogRef = this.dialog.open(dialogComponent, {
      width: widthInPx,
      data: data,
      panelClass: 'fullscreen-dialog',
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      callback.onDialogClosed(result);
    });
  }
}

