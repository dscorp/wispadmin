export class Plan {
  id: string;
  nombre: string;
  precio: number;
  velocidadSubida: number;
  velocidadBajada: number;
}
