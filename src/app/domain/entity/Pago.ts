import {MetodosPago} from './MetodosPago';

export class Pago {
  id: string;
  deuda: number;
  fecha: Date;
  descuento: number;
  codigo: string;
  motivo: string;
  montoAbonado: number;
  pagado: boolean;
  idSuscripcion: string;
  nombreCliente: string;
  telefonoCLiente: string;
  lugarCliente: string;
  direccionCliente: string;
  metodo: MetodosPago;
  encargado: string;
  modificadopor: string;
}
