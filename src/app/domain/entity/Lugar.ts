export class Lugar {
  id: string;
  nombre: string;
  abreviatura:string;
  lat: string;
  long: string;
}
