export class Suscripcion {
  id: string;
  nombreCompleto: string;
  telefono: string;
  direccion: string;
  lugar: string;
  nombrePlan: string;
  precioPlan: string;
  velocidadSubida: number;
  velocidadBajada: number;
  fechaSuscripcion: Date;
  servicioSuspendido: boolean;
  isNew: boolean;
  codigo:string;


  constructor(id: string, nombreCompleto: string, telefono: string, direccion: string, lugar: string, nombrePlan: string, precioPlan: string, velocidadSubida: number, velocidadBajada: number, fechaSuscripcion: Date, servicioSuspendido: boolean, isNew: boolean) {
    this.id = id;
    this.nombreCompleto = nombreCompleto;
    this.telefono = telefono;
    this.direccion = direccion;
    this.lugar = lugar;
    this.nombrePlan = nombrePlan;
    this.precioPlan = precioPlan;
    this.velocidadSubida = velocidadSubida;
    this.velocidadBajada = velocidadBajada;
    this.fechaSuscripcion = fechaSuscripcion;
    this.servicioSuspendido = servicioSuspendido;
    this.isNew = isNew;
  }
}
