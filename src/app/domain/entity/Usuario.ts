import {UsuarioTypes} from './UsuarioTypes';

export class Usuario {
  name: string;
  lastname: string;
  username: string;
  password: string;
  verified: boolean;
  id: string;
  type: UsuarioTypes;
}
