export interface Egreso {
  id:string,
  persona: string,
  motivo: string,
  monto: number,
  fecha: number,
}
