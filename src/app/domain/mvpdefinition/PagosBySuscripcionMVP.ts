import {Suscripcion} from '../entity/Suscripcion';
import {NetworkCallBack} from '../entity/NetworkCallBack';
import {Pago} from '../entity/Pago';
import {Subscription} from 'rxjs';

export module PagosBySuscripcionMVP {

  export abstract class Model {
    abstract getPagosBySuscripcion(suscripcion: Suscripcion, callback: NetworkCallBack<Pago[]>):Subscription

    abstract savePago(pago: Pago, callback: NetworkCallBack<Pago>);

    abstract  updatePago(pago: Pago, callback: NetworkCallBack<Pago>)
  }

  export abstract class View {

    abstract onPagosBySuscripcionReady(pagos:Pago[])

    abstract savePago(pago: Pago)

    abstract showInfoDialog(message: string)

    abstract showErrorDialog(message: string)

   abstract showLoading(show: boolean)

    abstract  disableBtnSavePago(b: boolean)
  }

  export abstract class Presenter {

    abstract setView(view: PagosBySuscripcionMVP.View)

    abstract getPagosBySuscripcion(suscripcion: Suscripcion)

    abstract savePagoBySuscripcion(pago: Pago)

    abstract unsuscribePagosBySuscription()

    abstract updatePago(pago: Pago)
  }

}
