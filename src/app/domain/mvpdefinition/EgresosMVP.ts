import {Egreso} from '../entity/Egreso';
import {NetworkCallBack} from '../entity/NetworkCallBack';

export module EgresosMVP {


  export abstract class Model {
   abstract loadEgresosByDate(startDate, endingDate, callback: NetworkCallBack<Egreso[]>): void;
  }

  export abstract class View {
    abstract onloadEgresosByDateRangeReady(egresos: Egreso[])

   abstract showErrorDialog(message: string): void;
  }

  export abstract class Presenter {

   abstract setView(param: EgresosMVP.View): void;

   abstract loadEgresosByDate(startDate, endingDate): void;
  }

}
