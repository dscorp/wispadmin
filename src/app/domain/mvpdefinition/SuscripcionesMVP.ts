import {Suscripcion} from '../entity/Suscripcion';
import {NetworkCallBack} from '../entity/NetworkCallBack';
import {Pago} from '../entity/Pago';
import {DialogFactory} from '../../presentation/DialogFactory';

export module SuscripcionesMVP {

  export abstract class Model {
    abstract saveSuscripcion(suscripcion: Suscripcion, callback: NetworkCallBack<Suscripcion>);

    abstract updateSuscripcion(suscripcion: Suscripcion, callback: NetworkCallBack<boolean>);

    abstract getAllSuscripcion(callback: NetworkCallBack<Suscripcion[]>)

    abstract getBySuscripcion(idSuscripcion: string, callback: NetworkCallBack<Pago[]>)

    abstract savePago(pago: Pago, callback: NetworkCallBack<Pago>)

    abstract getSuscripcionesConCorte(callback: NetworkCallBack<Suscripcion[]>)

  }


  export abstract class View {

    abstract showInfoDialog(message: string)

    abstract showErrorDialog(message: string)

    abstract onSuscripcionesListReady(result: Suscripcion[])

    abstract getSuscripcionesConCorte()

    abstract onSuscripcionesConCorteReady(suscripciones: Suscripcion[])

    abstract showLoadingDialog(show:boolean)
  }

  export abstract class Presenter {

    abstract setView(view: SuscripcionesMVP.View)

    abstract saveSuscripcion(suscripcion: Suscripcion)

    abstract getAllSuscripcion()

    abstract cortarServicio(suscripcion: Suscripcion)

    abstract reactivarServicio(suscripcion: Suscripcion)

    abstract updateSuscription(suscripcion: Suscripcion)

    abstract getSuscripcionesConCorte()

  }


}
