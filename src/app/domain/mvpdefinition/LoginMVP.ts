import { NetworkCallBack } from "src/app/domain/entity/NetworkCallBack";
import { Usuario } from "src/app/domain/entity/Usuario";

export module LoginMVP {

  export abstract class Model {
    abstract saveUser(user: Usuario, callback: NetworkCallBack<Usuario>);
    abstract signIn(username:string,password:string,callback: NetworkCallBack<Usuario>)
  }
  export abstract class View {
    abstract onSaveUserSuccess(user: Usuario)
    abstract onSaveUserFails(user?: Usuario, message?: String)
    abstract onSignInSuccessfull(user:Usuario)
    abstract onCredentialsDoesntMatch(message?:String)
    abstract onSignInFail(user?:Usuario, message?:String)
    abstract  showLoadingDialog(showDialog:boolean)
    abstract  disableSaveButton(b: boolean)
  }
  export abstract class Presenter {
    abstract setView(view: LoginMVP.View)
    abstract saveUser(user: Usuario);
    abstract signIn(username:string,password:string)
  }

}
