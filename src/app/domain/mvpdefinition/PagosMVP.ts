import {Pago} from '../entity/Pago';
import {NetworkCallBack} from '../entity/NetworkCallBack';

export module PagosMVP {

  export abstract class Model {
    abstract getByDateRange(date1: number, date2: number, callback: NetworkCallBack<Pago[]>)
  }

  export abstract class View {



    onPagosByDateRangeReady(result: Pago[]) {

    }

    showErrorDialog(message: string) {

    }
  }

  export abstract class Presenter {

    abstract setView(view: PagosMVP.View)

    abstract getPagosByDateRange(fechafinal: any, fechafinal2: any)
  }

}
