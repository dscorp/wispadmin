import {Suscripcion} from '../entity/Suscripcion';
import {NetworkCallBack} from '../entity/NetworkCallBack';

export abstract class SuscripcionRepository {

  abstract save(suscripcion: Suscripcion, callback: NetworkCallBack<Suscripcion>);

  abstract update(suscripcion: Suscripcion, callback: NetworkCallBack<boolean>);

  abstract getAll(callback: NetworkCallBack<Suscripcion[]>)

  abstract getSuscripcionesConCorte(callback: NetworkCallBack<Suscripcion[]>)
}
