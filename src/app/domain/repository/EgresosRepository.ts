import {NetworkCallBack} from '../entity/NetworkCallBack';
import {Egreso} from '../entity/Egreso';

export abstract class EgresosRepository {
  abstract loadEgresosByDate(startDate, endingDate, callback: NetworkCallBack<Egreso[]>): void;

  abstract save(value: Egreso, callback: NetworkCallBack<boolean>)
}
