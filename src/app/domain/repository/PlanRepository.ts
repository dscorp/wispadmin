import {NetworkCallBack} from '../entity/NetworkCallBack';
import {Plan} from '../entity/Plan';

export abstract class PlanRepository {

  abstract getAll(callback: NetworkCallBack<Plan[]>)

}
