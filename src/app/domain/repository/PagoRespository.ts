import {Pago} from '../entity/Pago';
import {NetworkCallBack} from '../entity/NetworkCallBack';

export abstract class PagoRespository {
  abstract save(pago: Pago,callback:NetworkCallBack<Pago>)

  abstract update(pago: Pago, callback: NetworkCallBack<Pago>)

  abstract getBySuscripcion(idSuscripcion: string, callback: NetworkCallBack<Pago[]>)

  abstract getByDateRange(date1: any, date2: any, callback: NetworkCallBack<Pago[]>)

}
