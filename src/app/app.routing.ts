import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './presentation/features/login/login.component';
import {BodyComponent} from './presentation/features/body/body.component';
import {AdminModule} from './framework/ngmodules/admin/admin.module';
import {AuthGuardGuard} from './framework/guards/auth-guard.guard';


const routes: Routes = [

  {
    path: '', component: LoginComponent
  },


  {
    path: 'admin',
    component: BodyComponent,
    canActivate: [AuthGuardGuard],
    loadChildren: () => import('./framework/ngmodules/admin/admin.module').then(m => AdminModule)

  }


];
export const APP_ROUTES = RouterModule.forRoot(routes);
