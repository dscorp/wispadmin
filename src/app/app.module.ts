import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {PagosMVP} from './domain/mvpdefinition/PagosMVP';
import {SuscripcionesMVP} from './domain/mvpdefinition/SuscripcionesMVP';
import {PagosBySuscripcionMVP} from './domain/mvpdefinition/PagosBySuscripcionMVP';
import {PagosBySuscripcionModel} from './data/model/PagosBySuscripcionModel';
import {SuscripcionesModel} from './data/model/SuscripcionesModel';
import {PagosModel} from './data/model/PagosModel';
import {SuscripcionesPresenter} from './presentation/features/suscripciones/SuscripcionesPresenter';
import {PagosBySyscripcionPresenter} from './presentation/features/suscripciones/pagos/pagos-by-suscripcion-list/PagosBySyscripcionPresenter';
import {SuscripcionRepositoryImpl} from './data/repository/SuscripcionRepositoryImpl';
import {PagoRepositoryImpl} from './data/repository/PagoRepositoryImpl';
import {PagoRespository} from './domain/repository/PagoRespository';
import {SuscripcionRepository} from './domain/repository/SuscripcionRepository';
import {SaveSuscripcionDialogComponent} from './presentation/features/suscripciones/save-suscripcion-dialog/save-suscripcion-dialog.component';
import {environment} from '../environments/environment';
import {PagosBySuscripcionDialogListComponent} from './presentation/features/suscripciones/pagos/pagos-by-suscripcion-list/pagos-by-suscripcion-dialog-list.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {APP_ROUTES} from './app.routing';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {PagosComponent} from './presentation/features/pagos/pagos.component';
import {DashboardComponent} from './presentation/features/dashboard/dashboard/dashboard.component';
import {SuscripcionesComponent} from './presentation/features/suscripciones/suscripciones.component';
import {SavePagoDlgComponent} from './presentation/features/suscripciones/pagos/save-pago-dlg/save-pago-dlg.component';
import {registerLocaleData} from '@angular/common';
import localePe from '@angular/common/locales/es-PE';
import {UpdateSuscripcionDialogComponent} from './presentation/features/suscripciones/update-suscripcion-dialog/update-suscripcion-dialog.component';
import {DropdownPlanesComponent} from './presentation/features/suscripciones/sharedComponents/dropdown-planes/dropdown-planes.component';
import {MatSelectModule} from '@angular/material/select';
import {PlanRepository} from './domain/repository/PlanRepository';
import {PlanRepositoryImpl} from './data/repository/PlanRepositoryImpl';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {LugarRepository} from './domain/repository/LugarRepository';
import {LugarRepositoryImpl} from './data/repository/LugarRepositoryImpl';
import {DropdownLugaresComponent} from './presentation/features/suscripciones/sharedComponents/dropdown-lugares/dropdown-lugares.component';
import {MatRadioModule} from '@angular/material/radio';
import {PagosPresenter} from './presentation/features/pagos/PagosPresenter';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {EgresosComponent} from './presentation/features/egresos/egresos.component';
import {SaveEgresoDialogComponent} from './presentation/features/egresos/save-egreso-dialog/save-egreso-dialog.component';
import {EgresosMVP} from './domain/mvpdefinition/EgresosMVP';
import {EgresosRepositoryImpl} from './data/repository/EgresosRepositoryImpl';
import {EgresosRepository} from './domain/repository/EgresosRepository';
import {EgresosModel} from './data/model/EgresosModel';
import {EgresosPresenter} from './presentation/features/egresos/EgresosPresenter';
import { LoginComponent } from './presentation/features/login/login.component';
import {LoginMVP} from './domain/mvpdefinition/LoginMVP';
import {LoginModel} from './data/model/LoginModel';
import {LoginPresenter} from './presentation/features/login/LoginPresenter';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import { BodyComponent } from './presentation/features/body/body.component';
import {DialogFactory} from './presentation/DialogFactory';
import { DropdownMeotodospagoComponent } from './presentation/features/suscripciones/sharedComponents/dropdown-meotodospago/dropdown-meotodospago.component';
import { UpdatePagoDlgComponent } from './presentation/features/suscripciones/pagos/update-pago-dlg/update-pago-dlg.component';
import { DetailPagoFPrintDlgComponent } from './presentation/features/suscripciones/pagos/detail-pago-f-print-dlg/detail-pago-f-print-dlg.component';
import {DialogUtil} from './presentation/dialog-util.service';

registerLocaleData(localePe, 'es-PE');

@NgModule({
  declarations: [
    AppComponent,
    SuscripcionesComponent,
    DashboardComponent,
    PagosComponent,
    SaveSuscripcionDialogComponent,
    PagosBySuscripcionDialogListComponent,
    SavePagoDlgComponent,
    UpdateSuscripcionDialogComponent,
    DropdownPlanesComponent,
    DropdownLugaresComponent,
    EgresosComponent,
    SaveEgresoDialogComponent,
    LoginComponent,
    BodyComponent,
    DropdownMeotodospagoComponent,
    UpdatePagoDlgComponent,
    DetailPagoFPrintDlgComponent,

  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    BrowserAnimationsModule,
    MatTabsModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatRadioModule,
    MatCardModule,
    MatIconModule,
    MatToolbarModule,
    MatMenuModule,
  ],
  providers: [

    //REPOSITORIES
    {provide: PagoRespository, useClass: PagoRepositoryImpl},
    {provide: PlanRepository, useClass: PlanRepositoryImpl},
    {provide: SuscripcionRepository, useClass: SuscripcionRepositoryImpl},
    {provide: LugarRepository, useClass: LugarRepositoryImpl},

    //MVP

    //MODEL

    //PAGOS GENERAL
    {provide: PagosMVP.Model, useClass: PagosModel},

      //LOGIN
      {provide: LoginMVP.Model, useClass: LoginModel},

      //PAGOSBYSUSCRIPCIONLIST

      {provide: SuscripcionesMVP.Model, useClass: SuscripcionesModel},

      {provide: PagosBySuscripcionMVP.Model, useClass: PagosBySuscripcionModel},

    //PRESENTER
    {provide: PagosMVP.Presenter, useClass: PagosPresenter},

    {provide: LoginMVP.Presenter, useClass: LoginPresenter},

    {provide: SuscripcionesMVP.Presenter, useClass: SuscripcionesPresenter},

    {provide: PagosBySuscripcionMVP.Presenter, useClass: PagosBySyscripcionPresenter},
    {provide: DialogFactory, useClass: DialogFactory},

    //Egresos
    {provide: EgresosRepository, useClass: EgresosRepositoryImpl},
    {provide: EgresosMVP.Model, useClass: EgresosModel},
    {provide: EgresosMVP.Presenter, useClass: EgresosPresenter},
    {provide:DialogUtil, useClass:DialogUtil},

    {provide: LOCALE_ID, useValue: 'es-PE'},


    //DIALOGS
    SaveSuscripcionDialogComponent,
    SavePagoDlgComponent,
    UpdateSuscripcionDialogComponent,
    SaveEgresoDialogComponent,
    UpdatePagoDlgComponent,
    DetailPagoFPrintDlgComponent,

    //DI LIKE HILT


  ]
  ,
  bootstrap: [AppComponent]
})
export class AppModule {
}
