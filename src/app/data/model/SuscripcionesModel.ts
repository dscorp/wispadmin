import {SuscripcionesMVP} from '../../domain/mvpdefinition/SuscripcionesMVP';
import {NetworkCallBack} from '../../domain/entity/NetworkCallBack';
import {Suscripcion} from '../../domain/entity/Suscripcion';
import {Pago} from '../../domain/entity/Pago';
import {Injectable} from '@angular/core';
import {PagoRespository} from '../../domain/repository/PagoRespository';
import {SuscripcionRepository} from '../../domain/repository/SuscripcionRepository';
import {AngularFirestore} from '@angular/fire/firestore';
import firebase from 'firebase';

@Injectable()
export class SuscripcionesModel implements SuscripcionesMVP.Model {

  constructor(
    private pagoRepository: PagoRespository,
    private suscripcionRepository: SuscripcionRepository,
    private firestore: AngularFirestore
  ) {
  }

  getSuscripcionesConCorte(callback: NetworkCallBack<Suscripcion[]>) {
    this.suscripcionRepository.getSuscripcionesConCorte(callback);
  }

  getAllSuscripcion(callback: NetworkCallBack<Suscripcion[]>) {
    this.suscripcionRepository.getAll(callback);
  }

  getBySuscripcion(idSuscripcion: string, callback: NetworkCallBack<Pago[]>) {
    this.pagoRepository.getBySuscripcion(idSuscripcion, callback);
  }

  savePago(pago: Pago, callback: NetworkCallBack<Pago>) {
    this.pagoRepository.save(pago, callback);
  }

  saveSuscripcion(suscripcion: Suscripcion, callback: NetworkCallBack<Suscripcion>) {
    const parent = this;
    this.incrementCounter('cliente', new class implements NetworkCallBack<void> {
      onComplete(result: void) {
        parent.firestore.collection('contadores').doc('cliente').get()
          .subscribe(response => {
            const contador = response.get('contador');
            const year: string = new Date().getFullYear().toString();
            const yearCode = year.substring(year.length - 2, year.length);
            const code = `${yearCode}${contador}`;
            suscripcion.codigo = code;

            parent.suscripcionRepository.save(suscripcion, callback);
          }, error => callback.onException(error));
      }

      onException(e: Error) {
        callback.onException(e);
      }
    });


  }

  updateSuscripcion(suscripcion: Suscripcion, callback: NetworkCallBack<boolean>) {
    this.suscripcionRepository.update(suscripcion, callback);
  }


  incrementCounter(counterDoc: string, callback: NetworkCallBack<void>) {
    this.firestore.collection('contadores').doc(counterDoc).update({
      contador: firebase.firestore.FieldValue.increment(1),
    }).then(() => {
      callback.onComplete();
    })
      .catch(error => {
        callback.onException(error);
      });

  }
}
