import {EgresosMVP} from '../../domain/mvpdefinition/EgresosMVP';
import {Injectable} from '@angular/core';
import {NetworkCallBack} from '../../domain/entity/NetworkCallBack';
import {Egreso} from '../../domain/entity/Egreso';
import {EgresosRepository} from '../../domain/repository/EgresosRepository';

@Injectable()
export class EgresosModel implements EgresosMVP.Model {

  constructor(private egresosRepository: EgresosRepository) {
  }

  loadEgresosByDate(startDate, endingDate, callback: NetworkCallBack<Egreso[]>): void {
      this.egresosRepository.loadEgresosByDate(startDate, endingDate, callback);
  }


}
