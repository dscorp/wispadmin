import {Injectable} from '@angular/core';
import {NetworkCallBack} from 'src/app/domain/entity/NetworkCallBack';
import {Usuario} from 'src/app/domain/entity/Usuario';
import {LoginMVP} from '../../domain/mvpdefinition/LoginMVP';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {UsuarioTypes} from '../../domain/entity/UsuarioTypes';


@Injectable()
export class LoginModel implements LoginMVP.Model {
  private reference: AngularFirestoreCollection;

  constructor(
    private firestore: AngularFirestore,
  ) {
    this.reference = firestore.collection('Users');
  }

  saveUser(user: Usuario, callback: NetworkCallBack<Usuario>) {
    const parent = this;
    parent.firestore.collection('Users', ref => ref
      .where('username', '==', user.username))
      .get().subscribe(response => {
      const arr = [];
      response.forEach(result => {
        arr.push(result.data());
      });
      if (arr.length == 0) {
        user.verified = false;
        user.type = UsuarioTypes.technical;
        parent.reference.add({...user})
          .then(callback.onComplete(user))
          .catch(error => callback.onException(error));
      } else {
        callback.onException(new Error('El usuario ya existe, si tiene problemas para registrarse contáctese con el área de soporte'));
      }
    }, error => callback.onException(error));
  }

  signIn(username: string, password: string, callback: NetworkCallBack<Usuario>) {
    this.firestore.collection('Users', ref => ref
      .where('username', '==', username)
      .where('password', '==', password)
    ).get().subscribe(response => {
      if (!response.empty) {
        const user = response.docs[0].data() as Usuario;
        user.password = '';
        user.id = response.docs[0].id;
        callback.onComplete(user);
      } else {
        callback.onComplete(null);
      }
    }, error => callback.onException(error));

  }


}
