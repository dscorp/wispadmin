import {PagosMVP} from '../../domain/mvpdefinition/PagosMVP';
import {NetworkCallBack} from '../../domain/entity/NetworkCallBack';
import {Pago} from '../../domain/entity/Pago';
import {Injectable} from '@angular/core';
import {PagoRespository} from '../../domain/repository/PagoRespository';

@Injectable()
export class PagosModel implements PagosMVP.Model {

  constructor(
    private pagoRepository: PagoRespository
  ) {
  }
  getByDateRange(date1: any, date2: any, callback: NetworkCallBack<Pago[]>) {
    this.pagoRepository.getByDateRange(date1, date2, callback);
  }

}
