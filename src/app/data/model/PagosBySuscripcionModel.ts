import {PagosBySuscripcionMVP} from '../../domain/mvpdefinition/PagosBySuscripcionMVP';
import {Suscripcion} from '../../domain/entity/Suscripcion';
import {NetworkCallBack} from '../../domain/entity/NetworkCallBack';
import {Pago} from '../../domain/entity/Pago';
import {Injectable} from '@angular/core';
import {PagoRespository} from '../../domain/repository/PagoRespository';
import {Subscription} from 'rxjs';

@Injectable()
export class PagosBySuscripcionModel implements PagosBySuscripcionMVP.Model {

  constructor(
    private repository: PagoRespository
  ) {
  }

  getPagosBySuscripcion(suscripcion: Suscripcion, callback: NetworkCallBack<Pago[]>):Subscription {
   return this.repository.getBySuscripcion(suscripcion.id, callback);
  }

  savePago(pago: Pago, callback: NetworkCallBack<Pago>) {
    this.repository.save(pago, callback);
  }

  updatePago(pago: Pago, callback: NetworkCallBack<Pago>) {
    this.repository.update(pago ,callback)
  }

}
