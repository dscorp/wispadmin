import {LugarRepository} from '../../domain/repository/LugarRepository';
import {Lugar} from '../../domain/entity/Lugar';
import {NetworkCallBack} from '../../domain/entity/NetworkCallBack';
import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable()
export class LugarRepositoryImpl implements LugarRepository {
  private collectionName: string = 'Lugares';

  constructor(
    private firestore: AngularFirestore
  ) {
  }

  getAll(callback: NetworkCallBack<Lugar[]>) {
    this.firestore.collection(this.collectionName).get()
      .subscribe(data => {
        const arr: Lugar[] = [];
        data.forEach(result =>
          arr.push(result.data() as Lugar)
        );
        callback.onComplete(arr);
      }, error => callback.onException(error));
  }

}
