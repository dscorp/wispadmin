import {EgresosRepository} from '../../domain/repository/EgresosRepository';
import {NetworkCallBack} from '../../domain/entity/NetworkCallBack';
import {Egreso} from '../../domain/entity/Egreso';
import {AngularFirestore} from '@angular/fire/firestore';
import {Injectable} from '@angular/core';

@Injectable()
export class EgresosRepositoryImpl implements EgresosRepository {
  private collectionName: string = 'Egresos';

  constructor(private firestore: AngularFirestore) {
  }

  loadEgresosByDate(startDate, endingDate, callback: NetworkCallBack<Egreso[]>): void {

    this.firestore.collection<Egreso>(this.collectionName,ref =>ref.
    where('fecha','>=',startDate)
    .where('fecha','<=',endingDate)).get().subscribe(response => {
      const arr: Egreso[] = [];
      response.forEach(result =>
        arr.push(result.data() as Egreso)
      );
      callback.onComplete(arr);
    }, error => {
      callback.onException(error);
    });

  }

  save(egreso: Egreso, callback: NetworkCallBack<boolean>) {
    const key = this.firestore.createId();
    egreso.id = key;
    this.firestore.collection(this.collectionName).doc(key).set(egreso)
      .then(()=>callback.onComplete(true))
      .catch(reason => callback.onException(reason))
  }

}
