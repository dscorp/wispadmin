import {SuscripcionRepository} from '../../domain/repository/SuscripcionRepository';
import {Suscripcion} from '../../domain/entity/Suscripcion';
import {NetworkCallBack} from '../../domain/entity/NetworkCallBack';
import {AngularFirestore} from '@angular/fire/firestore';
import {Injectable} from '@angular/core';


@Injectable()
export class SuscripcionRepositoryImpl implements SuscripcionRepository {

  constructor(
    private firestore: AngularFirestore,
  ) {
  }

  private collectionName = 'Suscripciones';

  async getAll(callback: NetworkCallBack<Suscripcion[]>) {

    this.firestore.collection<Suscripcion>(this.collectionName)
      .valueChanges({ idField: 'id' })
      .subscribe(snapshot => {
        let suscripciones = [];
        snapshot.forEach((doc) => {
          suscripciones.push(doc);
        });

        callback.onComplete(suscripciones);
      });
  }

  save(suscripcion: Suscripcion, callback: NetworkCallBack<Suscripcion>) {
    suscripcion.fechaSuscripcion = new Date(suscripcion.fechaSuscripcion);
    this.firestore.collection<Suscripcion>(this.collectionName).add(suscripcion)
      .then(callback.onComplete(suscripcion))
      .catch(error => callback.onException(error));

  }

  update(suscripcion: Suscripcion, callback: NetworkCallBack<boolean>) {

    this.firestore.collection(this.collectionName).doc(suscripcion.id).update(suscripcion)
      .then(callback.onComplete(true))
      .catch(error => callback.onException(error));
  }

  getSuscripcionesConCorte(callback: NetworkCallBack<Suscripcion[]>) {
    this.firestore.collection<Suscripcion[]>(this.collectionName, ref => ref.where('servicioSuspendido', '==', true))
      .get()
      .subscribe(data => {
        const suscripcionesConCorte = [];
        data.forEach(result => suscripcionesConCorte.push(result.data()));
        callback.onComplete(suscripcionesConCorte);
      }, error => callback.onException(error));

  }

}
