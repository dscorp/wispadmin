import {PagoRespository} from '../../domain/repository/PagoRespository';
import {NetworkCallBack} from '../../domain/entity/NetworkCallBack';
import {Pago} from '../../domain/entity/Pago';
import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Subscription} from 'rxjs';

@Injectable()
export class PagoRepositoryImpl implements PagoRespository {

  constructor(
    private firestore: AngularFirestore,
  ) {
  }

  private collectionName = 'Pagos';

  getByDateRange(date1: any, date2: any, callback: NetworkCallBack<Pago[]>) {
    this.firestore.collection<Pago[]>(this.collectionName, ref =>
      ref.where('fecha', '>', date1)
        .where('fecha', '<=', date2))
      .get()
      .subscribe(data => {
          const pagos = [];
          data.forEach(result => pagos.push(result.data()));
          callback.onComplete(pagos);
        },
        error => callback.onException(error));
  }

  save(pago: Pago, callback: NetworkCallBack<Pago>) {
    const id = this.firestore.createId();
    pago.id = id;
    this.firestore.collection(this.collectionName).doc(id).set(pago)
      .then(callback.onComplete(pago))
      .catch(error => callback.onException(error));
  }

  update(pago: Pago, callback: NetworkCallBack<Pago>) {
    this.firestore.collection(this.collectionName).doc(pago.id)
      .update(pago)
      .then(callback.onComplete(pago))
      .catch(error => callback.onException(error));
  }

  getBySuscripcion(idSuscripcion: string, callback: NetworkCallBack<Pago[]>): Subscription {

    return this.firestore.collection<Pago>(this.collectionName, ref =>
      ref.where('idSuscripcion', '==', idSuscripcion)
        .orderBy('fecha','desc'))
      .valueChanges()
      .subscribe(data => {

        console.log(data);
        callback.onComplete(data)},
        error => callback.onException(error));
  }


}
