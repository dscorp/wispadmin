import {PlanRepository} from '../../domain/repository/PlanRepository';
import {NetworkCallBack} from '../../domain/entity/NetworkCallBack';
import {Plan} from '../../domain/entity/Plan';
import {AngularFirestore} from '@angular/fire/firestore';
import {Injectable} from '@angular/core';


@Injectable()
export class PlanRepositoryImpl implements PlanRepository {

  private collectionName = 'Planes';

  constructor(private firestore: AngularFirestore) {
  }

  getAll(callback: NetworkCallBack<Plan[]>) {

    this.firestore.collection(this.collectionName,ref=>ref.orderBy("prioridad",'asc')).get().subscribe(data => {
      const planes = [];
      data.forEach(result => planes.push(result.data()));
      callback.onComplete(planes);

    }, error => {
      console.log(error);
      callback.onException(error)
    });
  }

}
