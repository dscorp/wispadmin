import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ADMIN_ROUTES} from './admin.routing';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
@NgModule({
  declarations: [


  ],
  imports: [
    CommonModule,
    ADMIN_ROUTES,
  ]
})
export class AdminModule { }
