import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from '../../../presentation/features/dashboard/dashboard/dashboard.component';
import {PagosBySuscripcionDialogListComponent} from '../../../presentation/features/suscripciones/pagos/pagos-by-suscripcion-list/pagos-by-suscripcion-dialog-list.component';


const coreRoutes: Routes = [
  {
    path: 'dashboard', component: DashboardComponent
  },

  {
    path: 'listaPagosSuscripcion', component: PagosBySuscripcionDialogListComponent
  },

];
export const ADMIN_ROUTES = RouterModule.forChild(coreRoutes);
