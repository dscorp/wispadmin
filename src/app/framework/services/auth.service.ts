import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Usuario} from '../../domain/entity/Usuario';
import {UsuarioTypes} from '../../domain/entity/UsuarioTypes';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user=new Usuario();


  constructor(private router: Router) {
    this.getUser();
  }

  singOut() {
    localStorage.clear();
    this.router.navigate(['']);
  }

  isAutenticated = () => (this.user.id != null && this.user.id != 'undefined') ? true : false;


   getUser() {
    const local_storage_user_id = localStorage.getItem('user_id');
    const name = localStorage.getItem('name');
    const lastname = localStorage.getItem('lastname');
     const type = localStorage.getItem('user_type');
    if (
      local_storage_user_id != 'undefinerd' && local_storage_user_id != null &&
      name != 'undefinerd' && name != null &&
      lastname != 'undefinerd' && lastname != null &&
      type != 'undefinerd' && type != null
    ) {
      this.user.id = local_storage_user_id;
      this.user.name = name;
      this.user.lastname = lastname;
      this.user.type=  UsuarioTypes[type]
    }
    return this.user;
  }


}
